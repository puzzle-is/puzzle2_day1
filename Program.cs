﻿using System;
class SinglyLinkedList 
{
    Node HeadA, HeadB;
 
    public class Node
     {
         public int data;
         public Node next;
 
        public Node(int P)
        {
            data = P;
            next = null;
        }
    }
    int getNode()
    {
        int c1 = getCount(HeadA);
        int c2 = getCount(HeadB);
        int P;
 
        if (c1 > c2)
         {
            P = c1 - c2;
            return _getIntesectingNode(P, HeadA, HeadB);
        }
        else
         {
            P = c2 - c1;
            return _getIntesectingNode(P, HeadA, HeadB);
        }
    }
    int _getIntesectingNode(int P, Node node1, Node node2)
    {
        int i;
        Node current1 = node1;
        Node current2 = node2;
        for (i = 0; i < P; i++) {
            if (current1 == null) {
                return -1;
            }
            current1 = current1.next;
        }
        while (current1 != null && current2 != null) {
            if (current1.data == current2.data) {
                return current1.data;
            }
            current1 = current1.next;
            current2 = current2.next;
        }
        return -1;
    }
 
    int getCount(Node node)
    {
        Node current = node;
        int count = 0;
 
        while (current != null) {
            count++;
            current = current.next;
        }
 
        return count;
    }
     public static void Main(String[] args)
    {
        SinglyLinkedList list = new SinglyLinkedList();
 
        //  first linked list
        list.HeadA = new Node(5);
        list.HeadA.next = new Node(8);
        list.HeadA.next.next = new Node(10);
        list.HeadA.next.next.next = new Node(13);
        list.HeadA.next.next.next.next = new Node(16);
 
        //  second linked list
        list.HeadB = new Node(4);
        list.HeadB.next = new Node(7);
        list.HeadB.next.next = new Node(13);
        list.HeadB.next.next.next = new Node(17);
 
        Console.WriteLine("Intersecting node is " + list.getNode());
    }
}